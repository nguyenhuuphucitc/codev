import 'package:flutter/material.dart';
import 'loginscreen1.dart';

class LoginScreen2 extends StatefulWidget {
  final String text;
  LoginScreen2({Key key, @required this.text}) : super(key: key);
  @override
  _LoginScreen2State createState() => new _LoginScreen2State(textemail: text);

}
// padding: new EdgeInsets.all(20.0),
// color: Colors.orangeAccent,
// child:
// new Image.asset('images/pika.png'),

class _LoginScreen2State extends State<LoginScreen2> {
  final TextEditingController _codeController = TextEditingController();
  String textemail;
  _LoginScreen2State({this.textemail});
  void  _showdialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(

        content: new Text("The Code is false"),
        actions: <Widget>[
          new FlatButton(
            child: new Text("OK"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    
      }
    );
  }
  void  _showdialog1(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(

        content: new Text("Register Success !!!"),
        actions: <Widget>[
          new FlatButton(
            child: new Text("OK"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    
      }
    );
  }
  void _submitRegister() async {
    String code = _codeController.text;
    if(code != "123456"){
       _showdialog();
    }else{
      _showdialog1();
      // Navigator.push(
      //   context, MaterialPageRoute(builder: (context) => LoginScreen2()));
    }
   
  }
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: new Scaffold(
          resizeToAvoidBottomPadding: false,
      body: new Stack(children: <Widget>[
        new Container(
          // color: Colors.blue,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: new Image.asset(
            'images/sanofi1.png',
            fit: BoxFit.cover,
          ),
        ),
        Container(
          alignment: new Alignment(0.0, -0.4),
          child: Container(
            width: 50.0,
            height: 50.0,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: new AssetImage(
                      'images/phuc.png',
                    ),
                    fit: BoxFit.cover),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
                boxShadow: [BoxShadow(blurRadius: 5.0, color: Colors.black)]),
          ),
        ),
        Container(
          alignment: new Alignment(0.0, -0.25),
          child: new Text(textemail),
        ),
        Padding(
        padding: const EdgeInsets.fromLTRB(0, 320, 0, 20),
        
        child: Container(
          
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            
            boxShadow: [BoxShadow(color: Color.fromRGBO(82, 92, 164, 1))],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
          child: TextField(
          enabled: false,
          // controller: _emailController,
          // style: TextStyle(fontSize: 18, color: Colors.black),
          decoration: InputDecoration(
              prefixIcon: Icon(Icons.lock , color : Colors.white),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xffCED0D2), width: 1),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
              )),
        ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(50, 320, 0, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            
            boxShadow: [BoxShadow(color: Colors.white)],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
          child: TextField(
          
          textAlign: TextAlign.center,
          controller: _codeController,
          // style: TextStyle(fontSize: 18, color: Colors.black),
          decoration: InputDecoration(
              hintText: 'Enter Code',
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xffCED0D2), width: 1),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
              )),
        ),
        ),
      ),
      
      
      new Padding(
        padding: const EdgeInsets.fromLTRB(0, 390, 0, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            boxShadow: [BoxShadow(color: Color.fromRGBO(82, 92, 164, 1))],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
        ),
      ),
      new Padding(
        padding: const EdgeInsets.fromLTRB(50, 390, 0, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width / 1.4,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Color.fromRGBO(202, 174, 122, 1))],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
          child: new FlatButton(
            onPressed: _submitRegister,
            child: Text("Register Now", style: TextStyle(color: Colors.white)),
          ),
        ),
      ),
      ]),
    ));
  }
}
// class TriangleClipper extends CustomClipper<Path> {
//   @override
//   Path getClip(Size size) {
//     final path = Path();
//     path.lineTo(0.0, size.height-(size.height/5));
//     path.lineTo(size.width/3, size.height);
//     path.lineTo(0.0, size.height);
//     path.close();
//     return path;
//   }

//   @override
//   bool shouldReclip(TriangleClipper oldClipper) => false;
// }
