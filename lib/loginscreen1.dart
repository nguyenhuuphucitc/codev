import 'package:flutter/material.dart';
import 'loginscreen2.dart';

class LoginScreen1 extends StatefulWidget {
  @override
  _LoginScreen1State createState() => new _LoginScreen1State();
}

class _LoginScreen1State extends State<LoginScreen1> {
  final TextEditingController _emailController = TextEditingController();
  // String erroremail (){
  //   String email = _emailController.text;
  //   Pattern pattern =
  //       r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    
  //   RegExp regex = new RegExp(pattern);
  //   String res ;
  //   if(email.isEmpty || email.length == 0){
  //     res = "email is not empty";
  //   }
  //   if (!regex.hasMatch(email)){
  //     res = "email invalidate";
  //   }
  //   return res;
  // }
  void  _showdialog(){
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
        content: new Text("The Email is false"),
        actions: <Widget>[
          new FlatButton(
            child: new Text("OK"),
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      );
    
      }
    );
  }
  void _submitlogin()  {
    String email = _emailController.text;
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(email)){
      _showdialog();
    }else{
      Navigator.push(
        context, MaterialPageRoute(builder: (context) => LoginScreen2(text: email)));
      
    }
  }

  @override
  Widget build(BuildContext context) {
    
    return new MaterialApp(
        home: new Scaffold(
          resizeToAvoidBottomPadding: false,
            body: new Stack(children: <Widget>[
      new Container(
        // color: Colors.blue,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: new Image.asset(
          'images/sanofi3.png',
          fit: BoxFit.cover,
        ),
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(0, 310, 0, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            
            boxShadow: [BoxShadow(color: Color.fromRGBO(82, 92, 164, 1))],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
          child: TextField(
          enabled: false,
          // controller: _emailController,
          // style: TextStyle(fontSize: 18, color: Colors.black),
          decoration: InputDecoration(
              suffixIcon: Icon(Icons.mail , color : Colors.white),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xffCED0D2), width: 1),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
              )),
        ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.fromLTRB(0, 310, 50, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            
            boxShadow: [BoxShadow(color: Colors.white)],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
          child: TextField(
          textAlign: TextAlign.center,
          controller: _emailController,
          // style: TextStyle(fontSize: 18, color: Colors.black),
          decoration: InputDecoration(
              hintText: 'Email Adress',
              // errorText: erroremail(),
              border: OutlineInputBorder(
                // borderSide: BorderSide(color: Color(0xffCED0D2), width: 1),
                borderRadius: BorderRadius.all(Radius.circular(75.0)),
              )),
        ),
        ),
      ),
      new Padding(
        padding: const EdgeInsets.fromLTRB(0, 390, 0, 20),
        child: Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 13.5,
          decoration: BoxDecoration(
            // rgb 82 92 164
            boxShadow: [BoxShadow(color: Color.fromRGBO(82, 92, 164, 1))],
            borderRadius: BorderRadius.all(Radius.circular(75.0)),
          ),
        ),
      ),
      new Padding(
        padding: const EdgeInsets.fromLTRB(50, 390, 0, 20),
        child: 
        Container(
          margin: new EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width / 1.4,
          height: MediaQuery.of(context).size.height / 13.5,
          // decoration: BoxDecoration(
            // boxShadow: [BoxShadow(color: Color.fromRGBO(202, 174, 122, 1))],
          //   borderRadius: BorderRadius.all(Radius.circular(75.0)),
          // ),
          child: new FlatButton(
            color: Color.fromRGBO(202, 174, 122, 1),
            shape: RoundedRectangleBorder(
             
            borderRadius: BorderRadius.all(Radius.circular(75.0)), 
            ),
            onPressed: _submitlogin,
            child: Text("CONTINUE...", style: TextStyle(color: Colors.white)),
          ),
        ),
      ),
    ])));
  }
}
